import React, {useRef, useEffect, useState} from 'react';
import ReactPlayer from '../node_modules/react-player';
import Video1 from "./asset/videos/v1_f1.mp4";
import Video2 from "./asset/videos/v2_f1.mp4";
import Video3 from "./asset/videos/v3_f1.mp4";
import Video4 from "./asset/videos/v4_f1.mp4";
import Video5 from "./asset/videos/v5_f1.mp4";
import CaptureVideoFrame from '../node_modules/capture-video-frame';




    return (
        <React.Fragment>
            <div className='player-wrapper' ref={wrapperRef} style={{ marginBottom: "2rem" }}>
                {/* <h1>{video_url}</h1> */}
                <ReactPlayer 
                    ref={videoRef}
                    url={video_objs[video_url]}
                    width="100%"
                    height="100%" 
                    playing={true} 
                    controls={true} 
                    muted={true}
                    progressInterval={250} 
                    onProgress={e => {
                        setVideoProgress(Math.round(e.playedSeconds * 1000));
                    }}
                />
            </div>
            <img ref={imgRef} ></img>
        </React.Fragment>
    );
}

export default Video;