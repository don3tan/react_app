  
import React, { useEffect, useRef } from "react";
import {
  select,
  scaleBand,
  axisBottom,
  stack,
  max,
  scaleLinear,
  axisLeft,
  stackOrderDescending
} from "d3";
import useResizeObserver from "./useResizeObserver";

function StackBarChart({data, keys, colors}){
    
    const svgRef = useRef();
    const wrapperRef = useRef();
    const dimensions = useResizeObserver(wrapperRef);

    useEffect(() => {

        keys = keys.filter(function(emotion_class){return emotion_class != "SelectAll"});
        const svg = select(svgRef.current);
        const {width, height} = 
            dimensions || wrapperRef.current.getBoundingClientRect();

        const stackGenerator = stack().keys(keys).order(stackOrderDescending);

        const layers = stackGenerator(data);
        // const extent = [0, max(layers, layer => max(layer, sequence => sequence[1]))]
        const extent = [0, 1]

        const xScale = scaleBand()
            .domain(data.map(d => d.time))
            .range([0, width]);

        const yScale = scaleLinear()
            .domain(extent)
            .range([height, 0])

        svg
            .selectAll(".layer")
            .data(layers)
            .join("g")
            .attr("class", "layer")
            .attr("fill", layer => {
                return colors[layer.key]
            })
            .selectAll("rect")
            .data(layer => layer)
            .join("rect")
            .attr("x", sequence => {
                return xScale(sequence.data.time);
            })
            .attr("width", xScale.bandwidth())
            .attr("y", sequence => yScale(sequence[1]))
            .attr("height", sequence => yScale(sequence[0]) - yScale(sequence[1]));
        
        const fixed_xScale = scaleBand()
        .domain(Array.from( {length: Math.round(data.slice(-1)[0].time)}, (x,i) => i ))
        .range([0, width]);
        const xAxis = axisBottom(fixed_xScale);
        svg
            .select(".x-axis")
            .attr("transform", `translate(0, ${height})`)
            .call(xAxis);

        const yAxis = axisLeft(yScale);
        svg
            .select(".y-axis")
            .call(yAxis.ticks(5));

    }, [data, dimensions, keys, colors]);
    
    return (
        <React.Fragment>
          <div ref={wrapperRef} style={{ marginBottom: "2rem" }}>
            <svg ref={svgRef}>
              <g className="x-axis" />
              <g className="y-axis" />
            </svg>
          </div>
        </React.Fragment>
      );

}

export default StackBarChart;