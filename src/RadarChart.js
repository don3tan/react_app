import React, { useRef, useEffect } from "react";
import { select, scaleOrdinal, schemeSet2, arc, pie, max, entries } from "d3";
import useResizeObserver from "./useResizeObserver";

function RadarChart({overall_data, selected_data, keys}){
    
    const svgRef = useRef();
    const wrapperRef = useRef();
    const dimensions = useResizeObserver(wrapperRef);

    const data = [
        {
            className:'Overall',
            axes: overall_data
        },
        {
            className:'Selected',
            axes: selected_data
        }
    ]

    useEffect(() => {

        let svg = select(svgRef.current);
        const {width, height} = 
            dimensions || wrapperRef.current.getBoundingClientRect();
        const margin = 40;
        const radius = Math.min(width,height) / 2 - margin;

    

    }, [overall_data, selected_data, dimensions]);
    
    return (
        <React.Fragment>
          <div ref={wrapperRef} style={{ height:"600px", marginBottom: "2rem" }}>
            <svg ref={svgRef}></svg>
          </div>
        </React.Fragment>
      );

}

export default RadarChart;