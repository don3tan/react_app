# docker.preferred.ai/my-npm-app-name

# Build the project first with a node container
FROM node:lts-slim AS build

# copy all project files to the docker container
COPY . /project
# set the working directory to the docker container folder
WORKDIR /project

# install npm requirements
RUN npm install --slient

# generate npm output files
RUN npm run build --prod

# This is the actual container
# Select a version to your liking
FROM nginx:stable

# Randomly set this between 501-999
ARG uid=993

RUN groupadd -g ${uid} -r app \
 && useradd -u ${uid} -r -g app -M app \
 && chown -R app:app /var/cache/nginx/ \
 && chown -R app:app /var/log/nginx \
 && rm -f /etc/nginx/conf.d/*

COPY docker/nginx.conf /etc/nginx/
COPY docker/app.conf /etc/nginx/conf.d/

# !!! If using angular it's /app/dist
COPY --from=build --chown=app:app /project/build /project

USER ${uid}
WORKDIR /project
